package l3m;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

//import java.sql.*;

public class DomPlatDAO extends DomDAO<Plat> {

	//public DomPlatDAO() {}

	@Override
	public boolean create (Plat obj) {
		return false;
	}
	@Override
	public boolean update (Plat obj) {
		return false;
	}

	@Override
	public boolean delete (Plat obj) {
		return false;
	}
	@Override 
	public Plat read(int id) {
            Plat p =null;
            try{
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

                DocumentBuilder builder = factory.newDocumentBuilder();
                File fileXML = new File("src/main/java/l3m/plats.xml");
                Document xml = builder.parse(fileXML);
                Element root = xml.getDocumentElement();
                XPathFactory xpf = XPathFactory.newInstance();
                XPath path = xpf.newXPath();
                String nom = (String) path.evaluate("//plat["+id+"]/id/text()", root,XPathConstants.STRING);
                String type = (String) path.evaluate("//plat["+id+"]/type/text()", root,XPathConstants.STRING);

                double prix =  Double.parseDouble((String) path.evaluate("//plat["+id+"]/prix/text()", root,XPathConstants.STRING));
                String photo = (String) path.evaluate("//plat["+id+"]/photo/text()", root,XPathConstants.STRING);
                
                
                NodeList list = (NodeList)path.evaluate("//plat["+id+"]/ingredients/ingredient/text()", root, XPathConstants.NODESET);

                List<String> ingredients = new ArrayList<String>();
                //Parcours de la boucle
                for(int i = 0 ; i < list.getLength(); i++){
                    ingredients.add(list.item(i).getTextContent());
                }

                p = new Plat(nom,type,prix,photo ,ingredients);

            } catch (ParserConfigurationException ex) {    
                Logger.getLogger(DomPlatDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(DomPlatDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DomPlatDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(DomPlatDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

            return p;
    }
    
    public int nbPlats() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
        
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            DocumentBuilder builder = factory.newDocumentBuilder();
            File fileXML = new File("src/main/java/l3m/plats.xml");
            Document xml = builder.parse(fileXML);
            Element root = xml.getDocumentElement();
            XPathFactory xpf = XPathFactory.newInstance();
            XPath path = xpf.newXPath();
            NodeList list = (NodeList)path.evaluate("//plat/id/text()", root, XPathConstants.NODESET);
            return list.getLength();

    }
}
