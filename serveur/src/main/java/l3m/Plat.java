package l3m;
import java.util.ArrayList;
import java.util.List;

public class Plat {

    private String id;
    private TypeDePlat type;
    private double prix;
    private String photo;
    private List<Ingredient> ingredients ;

    public Plat(){

    }

    public Plat(String id, double prix){

    }


    public Plat(String id, String type,double prix,String photo ,List<String> ingredients){
        this.id = id;
        switch(type){
            case "Entree":
                this.type = TypeDePlat.ENTREE;
                break;
            case "Plat":
            this.type = TypeDePlat.PLAT;
            break;
            case "Dessert":
                this.type = TypeDePlat.DESSERT;
                break;
            case "Boisson":
                this.type = TypeDePlat.BOISSON;
                break;

        }
        this.prix = prix;
        this.photo = photo;
        this.ingredients = new ArrayList<Ingredient>();
        for(String i : ingredients){
            switch(i){
                case ("Tomate"):
                  this.ingredients.add(Ingredient.TOMATE);
                  break;
                case ("Mozzarella"):
                  this.ingredients.add(Ingredient.MOZZARELLA);
                  break;
                case ("Fromage"):
                  this.ingredients.add(Ingredient.FROMAGE);
                  break;
                case ("Picorine"):
                  this.ingredients.add(Ingredient.PICORINE);
                  break;
                case ("Jambon"):
                  this.ingredients.add(Ingredient.JAMBON);
                  break;
                case ("Champ"):
                  this.ingredients.add(Ingredient.CHAMP);
                  break;
                case ("Saucisse italienne"):
                  this.ingredients.add(Ingredient.SAUCISSE_IT);
                  break;
                case ("Mortadelle"):
                  this.ingredients.add(Ingredient.MORTADELLE);
                  break;
                case ("Chevre"):
                  this.ingredients.add(Ingredient.CHEVRE);
                  break;
                case ("Roquefort"):
                  this.ingredients.add(Ingredient.ROQUEFORT);
                  break;
                case ("Mozza"):
                  this.ingredients.add(Ingredient.MOZZA);
                  break;
                case ("Poulet"):
                  this.ingredients.add(Ingredient.POULET);
                  break;
                case ("Chorizo"):
                  this.ingredients.add(Ingredient.CHORIZO);
                  break;
                case ("Oeuf"):
                  this.ingredients.add(Ingredient.OEUF);
                  break;
                case ("Lardons"):
                  this.ingredients.add(Ingredient.LARDONS);
                  break;
                case ("Rebloch"):
                  this.ingredients.add(Ingredient.REBLOCH);
                  break;
                case ("Thon"):
                  this.ingredients.add(Ingredient.THON);
                  break;
                case ("Anchois"):
                  this.ingredients.add(Ingredient.ANCHOIS);
                  break;
                case ("Capres"):
                  this.ingredients.add(Ingredient.CAPRES);
                  break;
                case ("Fruits de la mer"):
                  this.ingredients.add(Ingredient.FRUITS_DE_LA_MER);
                  break;
                case ("Aubergines"):
                  this.ingredients.add(Ingredient.AUBERGINES);
                  break;
                case ("Viande Hachee"):
                  this.ingredients.add(Ingredient.VIANDE_HACHEE);
                  break;
                case ("Ricotta creme"):
                  this.ingredients.add(Ingredient.RICOTTA_CREME);
                  break;
                case ("Creme"):
                  this.ingredients.add(Ingredient.CREME);
                  break;
                case ("Ravioles"):
                  this.ingredients.add(Ingredient.RAVIOLES);
                  break;
                case ("Saumon fume"):
                  this.ingredients.add(Ingredient.SAUMON);
                  break;
                case ("Ricotta"):
                  this.ingredients.add(Ingredient.RICOTTA);
                  break;
                case ("Merguez"):
                  this.ingredients.add(Ingredient.MERGUEZ);
                  break;
                case ("Sauce tomate"):
                  this.ingredients.add(Ingredient.SAUCE_TOMATE);
                  break;

            }
        }
    }

    public String getId() {
        return this.id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public TypeDePlat getType() {
        return this.type;
    }

    public void setType(TypeDePlat value) {
        this.type = value;
    }

    public double getPrix() {
        return this.prix;
    }

    public void setPrix(double value) {
        this.prix = value;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String value) {
        this.photo = value;
    }

    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(List<Ingredient> value) {
        this.ingredients = value;
    }
}
