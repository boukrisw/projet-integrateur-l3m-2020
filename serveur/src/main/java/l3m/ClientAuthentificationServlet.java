package l3m;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Je suis passé par l'itération 0 du serveur...
public class ClientAuthentificationServlet extends HttpServlet  {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println( this.processQueryTest(request) );
    }

	/*____________________________________________________________________________________________________________________
	 * doPost is expecting a HTTP parameter userId
	 * It sends back a XML serialization of the previous command with HTTP code 200 if a userId is specifyed
	 * It sends back a HTTP code 401 error if the userId is not specified or empty
	 * It sends back a HTTP code 500 error if a problem occured when accessing to the database
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
		// Extract userId from HTTP parameters
        //String userId = null;
		// Call the database and return result
		String Action = request.getParameter("action");

        try {
			/////********* Generation d'un id pour nouveau compte */
			int id=(int)(Math.random()*999);
			Statement requete = Connexion.getInstance().createStatement();
			ResultSet resultat = requete.executeQuery("select * from Client_ProjetINT where numCli="+id);
			while(resultat.next()){
				id=(int)(Math.random()*999);
				resultat = requete.executeQuery("select * from Client_ProjetINT where numCli="+id);
			}
			/******************************************************/
			response.setStatus(HttpServletResponse.SC_OK);
			boolean existe =false;

			if(Action.equals("connexion")){
				System.out.println("************Connexion************");				
				resultat = requete.executeQuery("select * from Client_ProjetINT where idFire='"+request.getParameter("idFire")+"'");

                if(!resultat.next()){
					System.out.println("************Client n'existe pas************");				
					Client c = new Client(""+id, request.getParameter("nom"),"", "NO_PIC", request.getParameter("email"), "Pas de tel" , "Pas d adresse");
					ClientDAO cDAO = new ClientDAO();
					cDAO.create(c);
					requete.executeUpdate("UPDATE Client_ProjetINT SET idFire='"+request.getParameter("idFire")+"' WHERE numCli="+id);
					response.getWriter().println(id);
				}else{
					System.out.println("************Client existe ************");
					//ecriture de la reponse pour le client!!!!!!!!!!!
					response.getWriter().println(resultat.getString("numCli"));
				}
				//response.getWriter().println( processQueryTest(request) );
			} else if(Action.equals("modification")){
				System.out.println("************modification************");
				
				resultat = requete.executeQuery("select * from Client_ProjetINT where idFire='"+request.getParameter("idFire")+"'");
                if(resultat.next()){
					System.out.println("************Client existe ************");
					//System.out.println("Id :"+resultat.getString("numCli")+ "  , nom :"+request.getParameter("nom") +" Prenom :"+request.getParameter("prenom") +" ;, tel :"+request.getParameter("tel")+", adresse :"+ request.getParameter("adresse"));
					Client c = new Client(resultat.getString("numCli"), request.getParameter("nom"),request.getParameter("prenom"), "NO_PIC", "email", request.getParameter("tel") , request.getParameter("adresse"));
					ClientDAO cDAO = new ClientDAO();
					boolean r = cDAO.update(c);
					response.getWriter().println(r);
					requete.close();
				}
			} else if(Action.equals("infoClient")){
				System.out.println("************infoClient************");
				System.out.println("Infos Client :"+request.getParameter("idFire"));
				resultat = requete.executeQuery("select * from Client_ProjetINT where idFire='"+request.getParameter("idFire")+"'");
                if(resultat.next()){
					System.out.println("************Client existe ************");
					String reponse = resultat.getString("nom")+","+resultat.getString("prenom")+","+resultat.getString("adresse")+","+resultat.getString("tel")+","+resultat.getString("photo")+","+resultat.getString("fidelite");
					response.getWriter().println(reponse);
				}
			}else if(Action.equals("infoLivraison")){
				System.out.println("************infoLivraison************");
				System.out.println("Infos Client :"+request.getParameter("idFire"));
				resultat = requete.executeQuery("select * from Client_ProjetINT where idFire='"+request.getParameter("idFire")+"'");
                if(resultat.next()){
					System.out.println("************Client existe ************");
					String reponse = resultat.getString("adresse")+","+resultat.getString("fidelite");
					response.getWriter().println(reponse);
				}
			}

			
			/*if(request.getParameter("type")!=null && request.getParameter("type").equals("commande")){
				System.out.println("Le prix est :" + request.getParameter("prix"));
				System.out.println("Le listfilms est :" + request.getParameter("listfilms").length());
			}
			
			String res = BdAccess.authentifyUser(userId);*/
        } catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println( e.toString() );
        }
    }

	private String processQueryTest(HttpServletRequest request) {
		String res = "{";
		Enumeration<String> P = request.getParameterNames();
		while (P.hasMoreElements()) {
			String p = P.nextElement();
			res += "\"" + p + "\": \"" + request.getParameter(p) + "\", ";
		}
		return res + "}";
	}

}
