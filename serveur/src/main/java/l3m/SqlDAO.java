package l3m;

import java.sql.*;

public abstract class SqlDAO<T> implements DAO<T> {

    protected Connection connect;
    public SqlDAO(){
        this.connect = Connexion.getInstance();
    }

	public abstract boolean create(T obj);
    public abstract T read(int id);
    public abstract boolean update(T obj);
    public abstract boolean delete(T obj);
}
