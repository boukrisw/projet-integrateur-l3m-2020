package l3m;

public enum TypeDePlat {

    ENTREE ("Entree"),
    PLAT ("Plat"),
    DESSERT ("Dessert"),
    BOISSON ("Boisson");

    private String type = "";
    
    //Constructeur
    TypeDePlat(String type){
        this.type = type;
    }
    
    public String toString(){
        return this.type;
    }
}
