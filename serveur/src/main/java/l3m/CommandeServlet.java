package l3m;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandeServlet extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/plain");
        
        String Action = request.getParameter("action");
	if(Action.equals("commande")){
            try {
                Statement requete = Connexion.getInstance().createStatement();
                ResultSet resultat;
                
                //response.getWriter().println( processQueryTest(request) );
                response.setStatus(HttpServletResponse.SC_OK);

                System.out.println("************AjoutCommande************");				
                
                /////********* Generation d'un id pour nouveau compte */
                int id=(int)(Math.random()*999);
                requete = Connexion.getInstance().createStatement();
                resultat = requete.executeQuery("select * from Commande_ProjetINT where numCommande="+id);
                while(resultat.next()){
                        id=(int)(Math.random()*999);
                        resultat = requete.executeQuery("select * from Commande_ProjetINT where numCommande="+id);
                }
                /******************************************************/

                
                double price =  Double.parseDouble(request.getParameter("prix"));
                
                LocalDateTime currentTime = LocalDateTime.now();
                String audd;
                audd = currentTime.toLocalDate().getDayOfMonth()+"-"+currentTime.toLocalDate().getMonthValue()+"-"+currentTime.toLocalDate().getYear();
                
                System.out.println("--------Commande--------");
                Commande c = new Commande(""+id, audd,request.getParameter("id"),request.getParameter("listplats") ,request.getParameter("listfilms"), price,request.getParameter("adresse") );
                System.out.println("------------------------");

                CommandeDAO cDAO = new CommandeDAO();
                cDAO.create(c);
            } catch (SQLException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().println( e.toString() );
            }
        }
    }

    private String processQueryTest(HttpServletRequest request) {
		String res = "{";
		Enumeration<String> P = request.getParameterNames();
		while (P.hasMoreElements()) {
			String p = P.nextElement();
			res += "\"" + p + "\": \"" + request.getParameter(p) + "\", ";
		}
		return res + "}";
	}
}