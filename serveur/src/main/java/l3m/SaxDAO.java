package l3m;
import javax.xml.parsers.SAXParser;

import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxDAO implements DAO<Plat>{

    protected SAXParser saxParser;

    @Override
    public boolean create(Plat obj) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Plat read (int id) {
        Plat p = new Plat();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            saxParser=factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean prix = false;

                public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException{
                    /*String id1 = Integer.toString(id);
                    if ("plat".equals(qName)) {
                    if(attr.equals(id1)) {*/
                    if (qName.equalsIgnoreCase("prix")) prix=true;
                }

                public void characters(char[] ch, int start, int length) throws SAXException{
                    if(prix) {
                        //p=new Plat(id,new String(ch, start, length));
                        prix=false;
                    }
                }
            };
            saxParser.parse("plats1.xml", handler);
        }catch(Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    @Override
    public boolean update(Plat obj) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean delete(Plat obj) {
        // TODO Auto-generated method stub
        return false;
    }

}