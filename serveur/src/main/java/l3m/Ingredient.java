package l3m;

public enum Ingredient {

    TOMATE("Tomate"),
    MOZZARELLA("Mozzarella"),
    FROMAGE("Fromage"),
    PICORINE("Picorine"),
    JAMBON("Jambon"),
    CHAMP("Champ"),
    SAUCISSE_IT("Saucisse italienne"),
    MORTADELLE("Mortadelle"),
    CHEVRE("Chevre"),
    ROQUEFORT("Roquefort"),
    MOZZA("Mozza"),
    POULET("Poulet"),
    CHORIZO("Chorizo"),
    OEUF("Oeuf"),
    LARDONS("Lardons"),
    REBLOCH("Rebloch"),
    THON("Thon"),
    ANCHOIS("Anchois"),
    CAPRES("Capres"),
    FRUITS_DE_LA_MER("Fruits de la mer"),
    AUBERGINES("Aubergines"),
    VIANDE_HACHEE("Viande Hachee"),
    RICOTTA_CREME("Ricotta creme"),
    CREME("Creme"),
    RAVIOLES("Ravioles"),
    SAUMON("Saumon fume"),
    RICOTTA("Ricotta"),
    MERGUEZ("Merguez"),
    SAUCE_TOMATE("Sauce tomate");

    private String ingredient = "";
    
    //Constructeur
    Ingredient(String ingredient){
        this.ingredient = ingredient;
    }
    
    public String toString(){
        return this.ingredient;
    }
}
