package l3m;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandeDAO extends SqlDAO<Commande> {
    
   public CommandeDAO(){
        super();
   } 

   @Override
   public boolean create(Commande obj) {
        System.out.println("Creaaaaaaaaaaaaaaaaaat");
        try {
            Statement requete = this.connect.createStatement();
            System.out.println("insert into Commande_ProjetINT values ("+obj.getId()+","+obj.getIdClient()+",TO_DATE('"+obj.getDate()+"', 'DD/MM/YYYY'),"+obj.getPrix()+",'"+obj.getAdresseLivraison()+"')");    
            requete.executeUpdate("insert into Commande_ProjetINT values ("+obj.getId()+","+obj.getIdClient()+",TO_DATE('"+obj.getDate()+"', 'DD/MM/YYYY'),"+obj.getPrix()+",'"+obj.getAdresseLivraison()+"')");
            obj.getIdFilms().forEach((f) -> {
                try {
                    requete.executeUpdate("insert into Commande_Film_ProjetINT values ('"+f+"',"+obj.getId()+")");
                } catch (SQLException ex) {
                    Logger.getLogger(CommandeDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            obj.getIdPlats().forEach((p) -> {
                try {
                    requete.executeUpdate("insert into Commande_Plat_ProjetINT values ("+p.idPlat+","+obj.getId()+","+p.quantite+")");
                } catch (SQLException ex) {
                    Logger.getLogger(CommandeDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return true;
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return false;
   }

   @Override
   public Commande read(int id) {
    Commande c = new Commande();
    try {
        ResultSet result = this.connect.createStatement().executeQuery("SELECT * FROM Commande WHERE NUMCOMMANDE = " + id);
        while(result.next()){    
            c = new Commande(id, result.getString("DATECOMMANDE"), result.getInt("PRIX"));
        }    
    } catch (SQLException e) {
         e.printStackTrace();
    }
    return c; 
   }

   @Override
   public boolean update(Commande obj) {
       //A completer
       try {
            Statement req = this.connect.createStatement();
            req.executeUpdate("UPDATE Commande_ProjetINT SET adresse='"+obj.getAdresseLivraison()+"' WHERE numCommande="+obj.getId());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
   }

   @Override
   public boolean delete(Commande obj) {
        //A completer
        try {
            Statement req = this.connect.createStatement();
            req.executeUpdate("DELETE FROM Commande_ProjetINT WHERE numCommande="+obj.getId());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}