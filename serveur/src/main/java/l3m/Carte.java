package l3m;
import java.util.List;
import java.util.ArrayList;

public class Carte {

    private List<Plat> plats;

    public List<Plat> getPlats() {
        if (this.plats == null) {
            this.plats = new ArrayList<Plat>();
        }
        return this.plats;
    }

    public void setPlats(List<Plat> plats){
      this.plats = plats;
    }

}
