package l3m;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.xml.sax.SAXException;

public class PlatsServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("application/json");
        
        response.setStatus(HttpServletResponse.SC_OK);
        int id = Integer.parseInt(request.getParameter("id"));
        DomPlatDAO dom =new DomPlatDAO();
        JSONArray list = new JSONArray();

        if(id == -1){
            
        }else{
            Plat p = dom.read(id);
            
              
            JSONObject plat = new JSONObject();

            JSONObject obj = new JSONObject();
            obj.put("plat", p.getId());
            obj.put("type", p.getType());
            obj.put("prix", p.getPrix());
            obj.put("imagePath", p.getPhoto());

            plat.put("infos", obj);
            
            
            ArrayList<String> arrayIngredients = new ArrayList<String>();
            for(Ingredient in : p.getIngredients()){
                arrayIngredients.add(in.toString());
            }
            
            JSONArray listIngredients = new JSONArray();
            for (int j = 0; j < arrayIngredients.size(); j++) {
                JSONObject objIngr = new JSONObject();
                objIngr.put("ingredient", arrayIngredients.get(j));
                listIngredients.add(objIngr);
            }
            plat.put("ingredients", listIngredients);

            list.add(plat);
        }
        response.getWriter().println(list);
    }
        

    


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/plain");
        
        response.setStatus(HttpServletResponse.SC_OK);
        int id = Integer.parseInt(request.getParameter("id"));
        DomPlatDAO dom =new DomPlatDAO();
        JSONArray list = new JSONArray();

        if(id == -1){
            try {
                int nbPlats = dom.nbPlats();
                for (int i=1; i<=nbPlats;i++ ){
                    Plat p = dom.read(i);
                    JSONObject plat = new JSONObject();

                    JSONObject obj = new JSONObject();
                    obj.put("plat", p.getId());
                    obj.put("type", p.getType().toString());
                    obj.put("prix", p.getPrix());
                    obj.put("imagePath", p.getPhoto());

                    plat.put("infos", obj);


                    ArrayList<String> arrayIngredients = new ArrayList<String>();
                    for(Ingredient in : p.getIngredients()){
                        arrayIngredients.add(in.toString());
                    }

                    JSONArray listIngredients = new JSONArray();
                    for (int j = 0; j < arrayIngredients.size(); j++) {
                        JSONObject objIngr = new JSONObject();
                        objIngr.put("ingredient", arrayIngredients.get(j));
                        listIngredients.add(objIngr);
                    }
                    plat.put("ingredients", listIngredients);
                    plat.put("id", i);
                    
                    list.add(plat);
                }
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(PlatsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(PlatsServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(PlatsServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Plat p = dom.read(id);
            
              
            JSONObject plat = new JSONObject();

            JSONObject obj = new JSONObject();
            obj.put("plat", p.getId());
            obj.put("type", p.getType().toString());
            obj.put("prix", p.getPrix());
            obj.put("imagePath", p.getPhoto());

            plat.put("infos", obj);
            
            
            ArrayList<String> arrayIngredients = new ArrayList<String>();
            for(Ingredient in : p.getIngredients()){
                arrayIngredients.add(in.toString());
            }
            
            JSONArray listIngredients = new JSONArray();
            for (int j = 0; j < arrayIngredients.size(); j++) {
                JSONObject objIngr = new JSONObject();
                objIngr.put("ingredient", arrayIngredients.get(j));
                listIngredients.add(objIngr);
            }
            plat.put("ingredients", listIngredients);
            plat.put("id", id);

            list.add(plat);
        }
        response.getWriter().println(list);
    }
}