drop table Commande_Plat_ProjetINT ;
drop table Commande_Film_ProjetINT ;
drop table Commande_ProjetINT ;
drop table Client_ProjetINT ;



create table Client_ProjetINT (
	numCli number(3),
	nom varchar2(20),
	prenom varchar2(20),
	adresse varchar2(50),
	email VARCHAR(320),
	tel varchar2(30),
	photo varchar2(100),
	fidelite number(3),
	idFire varchar2(100),
	CONSTRAINT PK_client_ProjetINT PRIMARY KEY (numCli)
);

create table Commande_ProjetINT (
	numCommande number(3),
	numCli number(3),
	dateCommande date,
	prix float(5),
	adresse varchar2(50),
	CONSTRAINT PK_Commande_ProjetINT PRIMARY KEY (numCommande),
	CONSTRAINT FK_Client_Commande_ProjetINT FOREIGN KEY (numCli) REFERENCES Client_ProjetINT (numCli)
);

create table Commande_Film_ProjetINT (
	idFilm varchar2(30),
	numCommande number(3),
	CONSTRAINT PK_Commande_Film_ProjetINT PRIMARY KEY (idFilm,numCommande),
	CONSTRAINT FK_Commande_Film_ProjetINT FOREIGN KEY (numCommande) REFERENCES Commande_ProjetINT (numCommande)
);

create table Commande_Plat_ProjetINT (
	idPlat number(3),	
	numCommande number(3),
	quantite number(3),
	CONSTRAINT PK_Commande_Plat_ProjetINT PRIMARY KEY (idPlat,numCommande),
	CONSTRAINT FK_Commande_Plat_ProjetINT FOREIGN KEY (numCommande) REFERENCES Commande_ProjetINT (numCommande)
);




--insert into Client_ProjetINT values (001,'sow','ousmane','30 rue X 38400 SMH','06','sow@hotmail.fr','pic_ouss.png',0);
