package l3m;

public class Client {

    private String id;
    private String nom;
    private String prenom;
    private String photo;
    private String email;
    private String tel;
    private String adresse;


    public Client(){
        
    }

    public Client(String id, String nom, String prenom, String photo, String email, String tel, String adresse){
        this.id=id;
        this.nom=nom;
        this.prenom=prenom;
        this.photo=photo;
        this.email=email;
        this.tel=tel;
        this.adresse=adresse;
    }
    public Client(int id, String nom, String prenom){
        
    }
    public String getId() {
        return this.id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String value) {
        this.nom = value;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String value) {
        this.prenom = value;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String value) {
        this.photo = value;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String value) {
        this.tel = value;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String value) {
        this.adresse = value;
    }
}
