package l3m;

import java.sql.*;


public class ClientDAO extends SqlDAO<Client> {

    public ClientDAO(){
         super();
    } 

    @Override
    public boolean create(Client obj) {
        //A completer
        try {
            Statement requete = this.connect.createStatement();
            requete.executeUpdate("insert into Client_ProjetINT values ("+obj.getId()+",'"+obj.getNom()+"','"+obj.getPrenom()+"','"+obj.getAdresse()+"','"+obj.getEmail()+"','"+obj.getTel()+"','"+obj.getPhoto()+"',0,'')");
            return true;
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return false;
    }

    @Override
    public Client read(int id) {
        Client cli = null;
        try {
            ResultSet result = this.connect.createStatement().executeQuery("SELECT * FROM Client_ProjetINT WHERE NUMCLIENT = " + id);
            //if(result.first())
            while(result.next()){    
                cli = new Client(id, result.getString("nom"), result.getString("prenom"));
            }    
            return cli;
        } catch (SQLException e) {
             e.printStackTrace();
        }
        return cli; 
    }

    @Override
    public boolean update(Client obj) {
        try {
            Statement req = this.connect.createStatement();
            req.executeUpdate("UPDATE Client_ProjetINT SET nom='"+obj.getNom()+"', prenom='"+obj.getPrenom()+"', tel='"+obj.getTel()+"',adresse='"+obj.getAdresse()+"' WHERE numCli="+obj.getId());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Client obj) {
        //A completer
        try {
            Statement req = this.connect.createStatement();
            req.executeUpdate("DELETE FROM Client_ProjetINT WHERE numCli="+obj.getId());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
