package l3m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Commande {

    private String id;
    private String date;
    private String idClient;
    private List<PlatQuantite> idPlats;
    private List<String> idFilms;
    private double prix;
    private String adresseLivraison;
    public Commande(){

    }
    
    public Commande(int id, String date , int prix){

    }
    public Commande(String id, String date,String idClient, String Plats ,String Films ,double prix, String adresseLivraison){
        this.id=id;
        this.date=date;
        this.idClient=idClient;
        
        
        //Les films!!!
        this.idFilms = new ArrayList<String>();
        if(!Films.equals("")){
            String[] tabFilms = Films.split(",");
            for (int i=0;i<tabFilms.length;i++){
                    //System.out.println("-------"+tabFilms[i]+"--------");
                    this.idFilms.add(tabFilms[i]);    
            }
        }
        
        //Les plats!!!
        this.idPlats = new ArrayList<PlatQuantite>();
        if(!Plats.equals("")){
            String[] tabPlats = Plats.split(";");
            for (int i=0;i<tabPlats.length;i++){
                String[] var = tabPlats[i].split(",");
                this.idPlats.add(new PlatQuantite(Integer.parseInt(var[0]),Integer.parseInt(var[1])));    
            }
        }
        
        this.prix=prix;
        this.adresseLivraison=adresseLivraison;
    }
    public String getId() {
        return this.id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String value) {
        this.date = value;
    }

    public String getIdClient() {
        return this.idClient;
    }

    public void setIdClient(String value) {
        this.idClient = value;
    }

    public List<PlatQuantite> getIdPlats() {
        return this.idPlats;
    }

    public void setIdPlats(List<PlatQuantite> value) {
        this.idPlats = value;
    }

    public List<String> getIdFilms() {
        return this.idFilms;
    }

    public void setIdFilms(List<String> value) {
        this.idFilms = value;
    }

    public double getPrix() {
        return this.prix;
    }

    public void setPrix(double value) {
        this.prix = value;
    }

    public String getAdresseLivraison() {
        return this.adresseLivraison;
    }

    public void setAdresseLivraison(String value) {
        this.adresseLivraison = value;
    }

    static class PlatQuantite {
        int idPlat;
        int quantite;

        public PlatQuantite(int idPlat,int quantite){
          this.idPlat=idPlat;
          this.quantite=quantite;
        }
    }
}
