package l3m;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;


public class TestDom { 
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        DomPlatDAO dom =new DomPlatDAO();
        System.out.println("b plats :"+dom.nbPlats());
        for(int i = 1; i < 10; i++){  
            System.out.println("-----------Plat id: "+i+"--------------");
            Plat p = dom.read(i);
            if(p != null){
                System.out.println("Plat :"+p.getId());
                System.out.println("prix :"+p.getPrix());
                System.out.println("photo :"+p.getPhoto());
                System.out.println("type :"+p.getType().toString());
                System.out.println("Ingredients :");

                for (Ingredient in: p.getIngredients()){
                    System.out.println("  - "+in.toString());
                }
            }else{
                System.out.println("Nulllllllllll");
            }
        }    
    }
}