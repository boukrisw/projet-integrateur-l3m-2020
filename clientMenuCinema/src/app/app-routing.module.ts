import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AccountComponent } from './account/account.component';
import { FilmsComponent } from './films/films.component';
import { FilmdetailsComponent } from './filmdetails/filmdetails.component';
import { PanierComponent } from './panier/panier.component';
import { AccueilComponent } from './accueil/accueil.component';
import { PlatsComponent } from './plats/plats.component';
const routes: Routes = [
  {path: 'login' , component: LoginComponent},
  {path: 'signup' , component: SignupComponent},
  {path: 'account' , component: AccountComponent},
  {path: 'films' , component: FilmsComponent},
  {path: 'filmdetails' , component: FilmdetailsComponent},
  {path: 'panier' , component: PanierComponent},
  {path: 'accueil' , component: AccueilComponent},
  {path: 'plats' , component: PlatsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

  export const routingComponents = [LoginComponent , SignupComponent , AccountComponent, FilmsComponent, FilmdetailsComponent, PanierComponent, AccueilComponent];
