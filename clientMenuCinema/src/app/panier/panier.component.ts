import { Component, OnInit } from '@angular/core';
import { PanierService } from '../services/panier/panier.service';
import { AuthService } from '../services/auth/auth.service';
import { PlatsService } from '../services/plats/plats.service';
import { PlatQte } from '../services/panier/panier.service';
@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {
  ps: PanierService;
  authPanier;
  

  constructor(public panierS: PanierService, public auth: AuthService, public platService : PlatsService) {
    if(this.panierS.adresseLivraison === null){
      auth.postLivraison().then(
        response => {
          const args = response.split(',');
          this.panierS.adresseLivraison = args[0];
          this.panierS.fidelite= parseInt(args[1]);
        }
      )
    }
    
    this.ps = panierS;
    this.authPanier = auth;
  }
  ngOnInit(): void {}

  initConfirm(): void {
    this.panierS.adresseLivraison = null;
    if(this.panierS.adresseLivraison === null){
      this.auth.postLivraison().then(
        response => {
          const args = response.split(',');
          this.panierS.adresseLivraison = args[0];
          this.panierS.fidelite= parseInt(args[1]);
        }
      )
    }
    this.panierS.prixTotal=0;
    this.panierS.listFilms=[];
    this.panierS.listPlats=[];
  }

  SuppFilm(id: number){
    this.ps.supprimerFilm( id);
  }

  async ConfirmerCommande(){
    console.log('*********');
    let Films = '';
    this.ps.listFilms.map(elt =>{
        Films= Films + elt.id+',' ;
      } 
    );
    
    if(Films !== '') Films = Films.substring(0,Films.length-1);
    console.log('*********',Films);

    let Plats = '';
    this.ps.listPlats.map(elt =>{
      Plats= Plats + elt.plat.id+','+elt.quantite+';' ;
      } 
    );
    if(Plats !== '') Plats = Plats.substring(0,Plats.length-1);
    console.log('*********',Plats);


    const reponseServeur = await this.authPanier.POST('/api/commande', {action: 'commande', adresse:  this.panierS.adresseLivraison,prix: this.ps.prixTotal, listfilms: Films,listplats :Plats, id : this.auth.id });
    this.initConfirm();
    console.log('Le serveur répond', reponseServeur);
  }


  DetailsPlat(id){
    console.log("details");
    let res
    let r = this.platService.postPlat(id).then( rrr => {
      
      res = rrr;      
    });
    console.log(res);

    return JSON.parse(res); 
    
  }

  enlever(plat : PlatQte){
    
    plat.quantite--;
    this.panierS.prixTotal -= plat.plat.infos.prix
    if(plat.quantite ===0){
        this.panierS.listPlats = this.panierS.listPlats.filter(function(v, index, arr){ return v.quantite > 0;});
    }
  }


  ajouter(plat :PlatQte){
    plat.quantite++;
    this.panierS.prixTotal += plat.plat.infos.prix
  }
}