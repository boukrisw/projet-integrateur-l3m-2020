import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import {PanierService} from "../services/panier/panier.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  pass;
  email;

  constructor(private authService: AuthService,panierS: PanierService) {
    panierS.ngOnInit();
  }

  logInWithGoogle() {
    this.authService.logInWithGoogle();
  }

  logInWithFacebook() {
    this.authService.logInWithFacebook();
  }

  ngOnInit(): void {
  }

  ConnectionClick() {
    this.authService.afAuth.signInWithEmailAndPassword(this.email, this.pass).catch(function(error) {
      // Handle Errors here.
      const errorMessage = error.message;
      console.log(errorMessage);
    });
  }
}
