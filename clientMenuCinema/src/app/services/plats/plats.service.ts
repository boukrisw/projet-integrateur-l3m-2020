import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { plat } from '../../data/platData';

@Injectable({
  providedIn: 'root'
})
export class PlatsService {

  plats:plat[] = [];
  plat

  http: HttpClient;
  constructor(http: HttpClient) {
    this.http = http;

  }

  async getData(options?) :Promise<plat[]>{
    const response = await this.http.get<plat[]>("/api/plats?id=1").toPromise();
    console.log("*********response*********");
    console.log(response);
    console.log("*********response*********");
    return <plat[]>response;
  }

  async postAllPlats() {
    await this.POSTALL('/api/plats', {id: "-1"});        
  }

  async postPlat(id: string) {
    
    const r = await this.POST('/api/plats', {id: "1"});
    let body = r.body;
    return body;
  }

  
  POSTALL(url: string, params: {[key: string]: string}) {
    const P = new HttpParams( {fromObject: params} );
    let res;
    this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise().then( (data) => {
        this.plats= JSON.parse(data.body);
      }
    );
  }

  POST(url: string, params: {[key: string]: string}) {
    const P = new HttpParams( {fromObject: params} );
    let res;
    return this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise();
  }
  
  /*
  POST(url: string, params: {[key: string]: string}) {
    const P = new HttpParams( {fromObject: params} );
    let res;
    this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise().then( (data) => {
        const p = JSON.parse(data.body);
        //console.log(data.body);
        res = p[0];
        console.log("*******res******");
        console.log(res);
        console.log("*******res******");
      }
    );
    return res;
  }*/
}
