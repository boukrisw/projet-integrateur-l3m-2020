import { Injectable } from '@angular/core';
import { TmdbService } from '../../tmdb.service';
import {MovieResponse} from "../../tmdb-data/Movie";
import { plat } from '../../data/platData';
@Injectable({
  providedIn: 'root'
})
export class PanierService {
  prixTotal;
  public listFilms: MovieResponse[]=[];
  public listPlats: PlatQte[];

  adresseLivraison = null;
  fidelite;

  tmbs: TmdbService;
  constructor(t: TmdbService) {
    this.tmbs= t;
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.prixTotal = 0;
    this.listFilms = [];
    this.listPlats = [];
  }

  async ajouterFilm(filmId: number) {
    const f= await this.tmbs.getMovie(filmId);
    this.listFilms.push(f);
    this.prixTotal += 3.75;
  }

  async supprimerFilm(filmId: number ){
    this.listFilms = this.listFilms.filter( f => f.id !== filmId);
    this.prixTotal -= 3.75;
  }

  panierVide(){
    return this.listFilms.length === 0 && this.listPlats.length === 0 ;
  }

  filmExiste(f: MovieResponse){
    let res = false;
    this.listFilms.forEach(film => {
        //console.log(" film id ",film.id);
        //console.log(" f id ",f.id);
        if(film!== undefined && f!==undefined &&  film.id === f.id) {
          res = true;
        }
    });
    return res;
  }

  containsPlat(plat : plat):PlatQte{
    let res = null;
    this.listPlats.forEach(p => {
        if(p.plat.id === plat.id) {
          res = p;
        }
    });
    return res;
  }

  enleverPlat(plat : plat){
    this.listPlats.forEach(p => {
        if(p.plat.id === plat.id) {
          p.quantite--;
          this.prixTotal -= p.plat.infos.prix;
          if(p.quantite === 0){
            this.listPlats = this.listPlats.filter(function(v, index, arr){ return v.quantite > 0;});
          }
        }
    });
  }

  ajouterPlat(plat : plat){
    const p = this.containsPlat(plat);
    if(p !== null){
      this.listPlats.forEach(p => {

        if(p.plat.id === plat.id) {
            p.quantite++;
            this.prixTotal += p.plat.infos.prix;
          }
      });
    }else{

      this.ajouterPlatFirst(plat ,1);
      this.prixTotal += plat.infos.prix;
    }
  }


  ajouterPlatFirst(plat : plat, qte: number) {
    this.listPlats.push( {plat: plat , quantite: qte} );
  }

}

export interface PlatQte {
  plat: plat;
  quantite: number;
} 
