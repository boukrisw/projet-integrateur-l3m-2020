import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

import { auth, User } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { SignupComponent } from '../../signup/signup.component';
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  Create=false;
  telCreate='';
  adresseCreate='';
  prenomCreate='';
  nomCreate=''

  
  uid = this.afAuth.authState.pipe(
    map(authState => {
      if (!authState) {
        return null;
      } else {
        return authState.uid;
      }
    }),
  );

  public id: number =-1;
  connected = false;
  user;
  public userDetails: User = null;
  uidUser: string ;
  http: HttpClient ;
  constructor(public afAuth: AngularFireAuth, http: HttpClient) {

    this.http = http;

    afAuth.user.subscribe();

    this.user = afAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
          this.connected = true ;
          this.uidUser = user.uid;
          if(user !==null){
            this.postConnexion().then( res => {
              if(res.C) this.postModification(res.n,res.p,res.t,res.a);
            });
          } 

        } else {
          this.userDetails = null;
        }
      }
    );
  }

  getUser(): User {
    return this.userDetails;
  }
  getIdUser(): string {
    return this.uidUser;
  }

  logInWithGoogle() {
    this.afAuth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  logInWithFacebook() {
    return this.afAuth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
  }

  signOut() {
    this.afAuth.signOut();
    this.connected = false;
    this.id = -1;
    this.Create=false;
  }

  async postConnexion() {
    if (this.userDetails !== null) {
      const reponseServeur = await this.POST('/api/authentification', {action:'connexion', nom: this.userDetails.displayName, email: ''+this.userDetails.email, idFire : this.userDetails.uid});
      console.log('Le serveur répond', reponseServeur);
      this.id = parseInt(reponseServeur.body); 
      console.log('id :', this.id);
      return {C:this.Create, n : this.nomCreate, p: this.prenomCreate, t:this.telCreate, a:this.adresseCreate};
    }
  }

  async postModification(nom, prenom, tel,adresse) {    
    if (this.userDetails !== null) {      
      const reponseServeur = await this.POST('/api/authentification', {action:'modification',nom: nom,prenom: prenom,adresse: adresse, tel: tel, idFire : this.userDetails.uid});
      console.log('Le serveur répond', reponseServeur);
      //return reponseServeur;
    }
  }

  async postInfoClient() {
    if (this.userDetails !== null) {
      const reponseServeur = await this.POST('/api/authentification', {action:'infoClient',idFire : this.userDetails.uid });
      return reponseServeur.body;
    }
  }

  async postLivraison() {
    if (this.userDetails !== null) {
      const reponseServeur = await this.POST('/api/authentification', {action:'infoLivraison',idFire : this.userDetails.uid });
      return reponseServeur.body;
    }
  }

  POST(url: string, params: {[key: string]: string}): Promise<HttpResponse<string>> {
    const P = new HttpParams( {fromObject: params} );
    return this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise();
  }
}
