import { TestBed } from '@angular/core/testing';

import { FilmdetailsService } from './filmdetails.service';

describe('FilmdetailsService', () => {
  let service: FilmdetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilmdetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
