import { Component, OnInit } from '@angular/core';
import {TmdbService} from '../tmdb.service';
import {FilmdetailsService} from '../services/filmdetails/filmdetails.service';
import {MovieResponse} from '../tmdb-data/Movie';
import { AuthService } from '../services/auth/auth.service'
import { PanierService } from '../services/panier/panier.service';

@Component({
  selector: 'app-filmdetails',
  templateUrl: './filmdetails.component.html',
  styleUrls: ['./filmdetails.component.scss']
})
export class FilmdetailsComponent implements OnInit {
  film: MovieResponse;

  constructor(private tmdb:TmdbService ,private fdetails: FilmdetailsService,public auth: AuthService,public ps: PanierService) {
    this.init() ;
  }

  existeDeja(){
    return this.ps.filmExiste(this.film);
  }

  async init() {
    this.film = await this.tmdb.getMovie(this.fdetails.idFilmDetail);
  }


  ngOnInit(): void {
  }

  recherche() {
    if (this.film !== undefined) {
      return true;
    }
    return false;
  }

  ajouterFilm(){
    this.ps.ajouterFilm(this.fdetails.idFilmDetail);
  }
  
  SuppFilm(){
    this.ps.supprimerFilm( this.fdetails.idFilmDetail);
  }
}
