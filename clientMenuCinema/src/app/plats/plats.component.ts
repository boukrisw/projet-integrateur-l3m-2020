import { Component, OnInit } from '@angular/core';
import { plat } from '../data/platData';
import { PlatsService } from '../services/plats/plats.service';
import { PanierService } from '../services/panier/panier.service';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-plats',
  templateUrl: './plats.component.html',
  styleUrls: ['./plats.component.scss']
})
export class PlatsComponent implements OnInit {

  plats:plat[] = [];

  constructor(public auth: AuthService, public platsService: PlatsService, public paniserService: PanierService) {

   }

  async ngOnInit() {
    //this.plats = await this.platsService.getData();
    //await this.platsService.postPlats("1");
    await this.platsService.postAllPlats();
  }

  clickForDetails(){

  }

  quantite(plat){
    const p = this.paniserService.containsPlat(plat);
    if(p !== null){
      return p.quantite;
    }else{
      return 0;
    }
  }

  enlever(plat){

    const p = this.paniserService.containsPlat(plat);
    if(p !== null){
      this.paniserService.enleverPlat(plat);
    }
  }


  ajouter(plat){
    this.paniserService.ajouterPlat(plat);
  }
}
