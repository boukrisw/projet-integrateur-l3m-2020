import { Component, OnInit } from '@angular/core';
import { Movies } from '../tmdb-data/Movie';
import { TmdbService } from '../tmdb.service';
import { environment } from '../../environments/environment';
import { SearchMovieQuery, SearchMovieResponse } from '../tmdb-data/searchMovie';
import { FilmdetailsService } from '../services/filmdetails/filmdetails.service'
import { PanierService } from '../services/panier/panier.service';
import { AuthService } from '../services/auth/auth.service';
@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})

export class FilmsComponent implements OnInit {
  searchQuery = '' ;
  RechercheTrouve = false;
  SMQuery: SearchMovieQuery;
  SMResponse: SearchMovieResponse;

  popularFilms: Movies;
  upComingFilms: Movies =undefined;
  topRatedFilms: Movies =undefined;
  constructor(public auth: AuthService, private tmdb: TmdbService, private fdetails: FilmdetailsService ,private ps:PanierService) {
    this.init();
  }

  existeDeja(f){
    return this.ps.filmExiste(f);
  }

  async init() {
    this.tmdb.init( environment.tmdbKey );
    this.popularFilms = await this.tmdb.popularFilms();
    this.upComingFilms = await this.tmdb.upComingFilms();
    this.topRatedFilms = await this.tmdb.topRatedFilms();
  }

  ngOnInit(): void {
  }

  async searchQueryClick() {
    this.SMResponse = await this.findMovies(this.searchQuery);
    this.RechercheTrouve = true;
  }

  findMovies(query: string) {
      const region = 'FR';
      this.SMQuery = {query, region};
      return this.tmdb.searchMovie(this.SMQuery);
  }

  popularFilmsExists() {
    if (this.popularFilms !== undefined) {
      return this.popularFilms.results.length > 0 ;
    }
    return false;
  }

  upComingFilmsExists() {
    if (this.upComingFilms !== undefined) {
      return this.upComingFilms.results.length > 0 ;
    }
    return false;
  }

  topRatedFilmsExists() {
    if (this.topRatedFilms !== undefined) {
      return this.topRatedFilms.results.length > 0 ;
    }
    return false;
  }

  clickForDetails(id: any) {
    this.fdetails.idFilmDetail = id ;
  }

  ajouterFilm(id: number){
    this.ps.ajouterFilm(id);
  }
  
  SuppFilm(id: number){
    this.ps.supprimerFilm(id);
  }
}
