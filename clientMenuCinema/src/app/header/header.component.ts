import { Component, OnInit } from '@angular/core';
import { AuthService } from './../services/auth/auth.service';
import { PanierService } from '../services/panier/panier.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private authService: AuthService,private panierService: PanierService) { }

  ngOnInit(): void {
  }

  isConnected() {
    return this.authService.connected;
  }
  logout() {
    this.panierService.listFilms = [];
    this.panierService.listPlats = [];
    this.authService.signOut();
  }
}

