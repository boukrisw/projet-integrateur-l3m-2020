export interface plat {
    ingredients: Ingredient[];
    infos:Information;
    id;
}


export interface Ingredient {
    ingredient: string;
}
export interface Information {
    
    prix;
    imagePath;
    plat;
    type;

}
