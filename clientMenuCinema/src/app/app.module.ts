import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import {TmdbService} from './tmdb.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { FilmsComponent } from './films/films.component';
import { FilmdetailsComponent } from './filmdetails/filmdetails.component';
import { PanierComponent } from './panier/panier.component';
import { AccueilComponent } from './accueil/accueil.component';
import { PlatsComponent } from './plats/plats.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    LoginComponent,
    FilmsComponent,
    FilmdetailsComponent,
    PanierComponent,
    AccueilComponent,
    PlatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase, 'project-772212420700'),
    AngularFireAuthModule,
    AngularFireAnalyticsModule,
    AngularFirestoreModule
  ],
  providers: [TmdbService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
