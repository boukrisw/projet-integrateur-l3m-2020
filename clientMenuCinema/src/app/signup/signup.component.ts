import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})

export class SignupComponent implements OnInit {
  pass='';
  email='';

  tel='';
  adresse='';
  prenom='';
  nom=''
  
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  CreatAcountClick() {
    this.authService.Create =true;
    this.authService.telCreate = this.tel;
    this.authService.adresseCreate = this.adresse;
    this.authService.prenomCreate =this.prenom;
    this.authService.nomCreate= this.nom;
    
    this.authService.afAuth.createUserWithEmailAndPassword(this.email, this.pass).catch( function(error) {
      // Handle Errors here.
      const errorMessage = error.message;
      console.log(errorMessage);
    });
  }
}
