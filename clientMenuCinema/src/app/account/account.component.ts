import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { User } from 'firebase/app';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  responseServeur;
  nom='';
  prenom='';
  adresse='';
  tel='';
  photo='';
  fidelite=0;

  uidDetails: User = null;
  uid: string;
  constructor(private authService: AuthService) {
    this.uidDetails = authService.getUser();
    this.uid = authService.getIdUser();
    authService.postInfoClient().then(
      response => {
        const args = response.split(',');
        this.nom=args[0];
        this.prenom=args[1];
        this.adresse=args[2];
        this.tel=args[3];
        this.photo=args[4];
        this.fidelite= parseInt(args[5]);
        this.responseServeur = response;
      }
    );


    
  }

  ngOnInit(): void {
  }
  changements(){
    this.authService.postModification(this.nom, this.prenom, this.tel, this.adresse);
  }
}
