// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tmdbKey: '200b64b2e13e6f6e004f310c0c0d8697',
  firebase: {
    apiKey: "AIzaSyBnkfAksIrCcA0Qrkj-BDHN3RFWqZ4ItQI",
    authDomain: "projet-menucinema.firebaseapp.com",
    databaseURL: "https://projet-menucinema.firebaseio.com",
    projectId: "projet-menucinema",
    storageBucket: "projet-menucinema.appspot.com",
    messagingSenderId: "772212420700",
    appId: "1:772212420700:web:1e9a21ce614c9735113699",
    measurementId: "G-NRHTFG480N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
